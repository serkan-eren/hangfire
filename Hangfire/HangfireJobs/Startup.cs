﻿using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HangfireJobs.Startup))]

namespace HangfireJobs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Db Bilgilerimizi bu alana giriyoruz.
            GlobalConfiguration.Configuration.UseSqlServerStorage(@"Server=Db\SQLEXPRESS;Database=Hangfire;Integrated Security=true");
            
            //Dashboardunu aktif ediyoruz. 
            app.UseHangfireDashboard();

            //Serverı aktif hale getiriyoruz.
            app.UseHangfireServer();
        }
    }
}
